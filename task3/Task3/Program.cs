﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Task3
{
    class Program
    {
        private static List<string> listOfNames = new List<string> {
            "Ola Norman",
            "Knut Sverre",
            "Geir Ola",
            "Anne Beate",
            "Ole Kristian"
        };
        private static bool done = false;
        static void Main(string[] args)
        {
            //loop until user exits
            while (!done)
            {
                string searchTerm = PromptUserForInput();
                ShowMatchingNames(searchTerm);
                CheckIfUserIsDone();
            }
        }

        private static void CheckIfUserIsDone()
        {
            //Function to check if user is done with the session
            Console.WriteLine();
            Console.WriteLine("Press q to quit, or any key to try again");
            if (Console.ReadKey().Key == ConsoleKey.Q) done = true;
        }

        private static void ShowMatchingNames(string searchTerm)
        {
            //Function that will clear the screen, then print all names matching query
            Console.Clear();
            Console.WriteLine("Here are the matching names:");
            foreach (string searchMatch in listOfNames.FindAll(s => s.Contains(searchTerm)))
            {
                Console.WriteLine(" - " + searchMatch);
            }
        }

        private static string PromptUserForInput()
        {
            //Function that prompts the user for input, and returns the string they type
            Console.Clear();
            Console.WriteLine("Pleas enter a search term:");
            string searchTerm = Console.ReadLine();
            return searchTerm;
        }
    }
}
